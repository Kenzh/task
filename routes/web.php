<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});
Route::get('/branches','BranchesController@index');
Route::get('/employees/name','EmployeesController@index');
Route::get('/employees/branch','EmployeesController@index');
Route::get('/employees/status','EmployeesController@index');
Route::post('/create/company','CompaniesController@create');
Route::post('/create/branches','BranchesController@create');
Route::post('/create/employee','EmployeesController@create');
