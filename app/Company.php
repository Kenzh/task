<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Company extends Model
{
    public static function insertCompany($name,$phone,$status,$text){
        DB::table('companies')->insertGetId(
            ['name' => $name, 'phone_num' => $phone,'status' => $status,'description' => $text]
        );
        return true;
    }

}
