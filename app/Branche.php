<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Branche extends Model
{
    public static function active(){

        return $branches = DB::table('branches')
            ->join('companies', 'branches.company_id', '=', 'companies.id')
            ->select('branches.*', 'companies.name as company_name', 'companies.status')
            ->where('status',1)
            ->get();
    }

    public static function insertBranch($name,$address,$company_id){
        DB::table('branches')->insertGetId(
            ['name' => $name, 'address' => $address,'company_id' => $company_id]
        );
        return true;
    }
}
