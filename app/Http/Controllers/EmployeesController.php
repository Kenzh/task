<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
class EmployeesController extends Controller
{
    public function index(Request $request){
        if($request->has('names')){
            $emp_name=$request->names;
            $employee=App\Employee::searchByName($emp_name);
            return json_encode($employee);
        }

        if($request->has('branch_name')){
            $br_name=$request->branch_name;
            $employee=App\Employee::searchByBranch($br_name);
            return json_encode($employee);
        }

        if($request->has('stat')){
            $status=$request->stat;
            $employee=App\Employee::searchByStatus($status);
            return json_encode($employee);
        }

    }

    public function create(Request $request){
        $employee_name=$request->name;
        $branch_id=$request->branch_id;

        $create=App\Employee::insertEmployee($employee_name,$branch_id);
        if ($create)
            return 'ok!';
    }


}
