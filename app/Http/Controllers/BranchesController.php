<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
class BranchesController extends Controller
{
    public function index(){
        $branches=App\Branche::active();
        return json_encode($branches);
    }

    public function create(Request $request){
        $branch_name=$request->name;
        $address=$request->address;
        $company_id=$request->company_id;

        $create=App\Branche::insertBranch($branch_name,$address,$company_id);
        if ($create)
            return 'ok!';
    }
}
