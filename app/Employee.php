<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    public static function searchByName($name){
        return $employee = DB::table('emp_branches')
            ->join('employees', 'emp_branches.employee_id', '=', 'employees.id')
            ->join('branches', 'emp_branches.branch_id', '=', 'branches.id')
            ->join('companies', 'branches.company_id', '=', 'companies.id')
            ->select('employees.name as employee_name', 'companies.name as company_name','companies.status', 'branches.name as branch_name','branches.address')
            ->where('employees.name',$name)
            ->get();
    }


    public static function searchByBranch($branch_name){
        return $employee = DB::table('emp_branches')
            ->join('employees', 'emp_branches.employee_id', '=', 'employees.id')
            ->join('branches', 'emp_branches.branch_id', '=', 'branches.id')
            ->join('companies', 'branches.company_id', '=', 'companies.id')
            ->select('employees.name as employee_name', 'companies.name as company_name','companies.status', 'branches.name as branch_name','branches.address')
            ->where('branches.name',$branch_name)
            ->get();
    }

    public static function searchByStatus($status){
        return $employee = DB::table('emp_branches')
            ->join('employees', 'emp_branches.employee_id', '=', 'employees.id')
            ->join('branches', 'emp_branches.branch_id', '=', 'branches.id')
            ->join('companies', 'branches.company_id', '=', 'companies.id')
            ->select('employees.name as employee_name', 'companies.name as company_name','companies.status', 'branches.name as branch_name','branches.address')
            ->where('companies.status',$status)
            ->get();
    }


    public static function insertEmployee($name,$branch_id){
       $id= DB::table('employees')->insertGetId(
            ['name' => $name]
        );
        DB::table('emp_branches')->insertGetId(
            ['employee_id' => $id,'branch_id'=>$branch_id]
        );
        return true;
    }

}
